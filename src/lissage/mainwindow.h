#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFileDialog>
#include <QMainWindow>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <vector>

namespace Ui {
class MainWindow;
}

using namespace OpenMesh;
using namespace OpenMesh::Attributes;

struct MyTraits : public OpenMesh::DefaultTraits
{
    // use vertex normals and vertex colors
    VertexAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color );
    // store the previous halfedge
    HalfedgeAttributes( OpenMesh::Attributes::PrevHalfedge );
    // use face normals face colors
    FaceAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color );
    EdgeAttributes( OpenMesh::Attributes::Color );
    // vertex thickness
    VertexTraits{float thickness; float value;};
    // edge thickness
    EdgeTraits{float thickness;};
};
typedef OpenMesh::TriMesh_ArrayKernelT<MyTraits> MyMesh;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    OpenMesh::Vec3f laplaceBeltrami (MyMesh* _mesh, int vertID); /// Application de l'opérateur de Laplace-Beltrami au point vertID
    void laplaceAll (MyMesh* _mesh); /// Applique l'opérateur de Laplace-Beltrami à tous les points et applanit les surfaces

    void displayMesh(MyMesh *_mesh, bool isTemperatureMap = false, float mapRange = -1);
    void resetAllColorsAndThickness(MyMesh* _mesh);
    void updateProgressBar(int value); /// Macro pour mettre à jour la barre de chargement

    // Utilisés pour le poids par contangente
    float faceArea(MyMesh* _mesh, int faceID); /// Donne l'aire de la face faceID
    float baryArea(MyMesh* _mesh, int vertID); /// Done l'aire barycentrique du point vertID

    // Utilisées pour du débug seulement - désactivées ici
    double wij(MyMesh* _mesh, int edgeID); /// Donne le poids à une arète spécifique (utilisé seulement dans matriceLB donc désactivé)
    void matriceLB (MyMesh* _mesh); /// Donne la matrice de Laplace-Beltrami (désactivé)

private slots:

    void on_pushButton_chargement_clicked();
    void on_pushButton_laplace_clicked();
    void on_pushButton_sauvegarde_clicked();

private:

    bool modevoisinage;

    MyMesh mesh;

    int vertexSelection;
    int edgeSelection;
    int faceSelection;

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
