#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <fstream>
#include <QMessageBox>

double cotan (double angle) { return cos(angle)/sin(angle); }

OpenMesh::Vec3f MainWindow::laplaceBeltrami (MyMesh* _mesh, int vertexID)
{
    VertexHandle vh = _mesh->vertex_handle ( vertexID );

    float poids;

    // Utilisé pour le poids par cotangente (donc désactivé)
    if (ui->comboBox_Laplace->currentIndex() == 0)
        poids = 1/(2*baryArea(_mesh,vertexID));
    else
        poids = 1;

    OpenMesh::Vec3f sum = {0,0,0};
    for (MyMesh::VertexEdgeIter curEdge = _mesh->ve_iter(vh) ; curEdge.is_valid() ; curEdge++)
    {
        MyMesh::EdgeHandle eh = *curEdge;
        MyMesh::HalfedgeHandle heh0 = _mesh->halfedge_handle(eh, 0);
        MyMesh::HalfedgeHandle heh1 = _mesh->halfedge_handle(eh, 1);

        MyMesh::VertexHandle vh2 = _mesh->to_vertex_handle(heh0);
        if (vh2 == vh)
            vh2 = _mesh->to_vertex_handle(heh1);

        Vec3f v  = _mesh->point(vh ), // C (et toujours correspondant à vertexID)
              vi = _mesh->point(vh2); // D (toujours l'autre point)

        float coeff;

        // Utilisé pour le poids par cotangente (donc désactivé)
        if (ui->comboBox_Laplace->currentIndex() == 0)
        {

            MyMesh::FaceHandle fh1 = _mesh->face_handle(heh0);
            MyMesh::FaceHandle fh2 = _mesh->face_handle(heh1);

            // Si l'arête est en bordure, on ne traite qu'une face
            if ( fh2.idx ( ) > _mesh->n_faces ( ) )
                fh2 = fh1;

            // Récupère les deux autres points des faces fh1 (CDA) et fh2 (CDB)
            MyMesh::VertexHandle vh3, vh4;

            for (MyMesh::FaceVertexIter curVert = _mesh->fv_iter(fh1) ; curVert.is_valid() ; curVert++)
            {
                MyMesh::VertexHandle vht = *curVert;
                if (vht != vh && vht != vh2)
                    vh3 = vht;
            }

            for (MyMesh::FaceVertexIter curVert = _mesh->fv_iter(fh2) ; curVert.is_valid() ; curVert++)
            {
                MyMesh::VertexHandle vht = *curVert;
                if (vht != vh && vht != vh2)
                    vh4 = vht;
            }

            Vec3f va = _mesh->point(vh3), // A
                  vb = _mesh->point(vh4); // B

            Vec3f vva = v-va,
                  viva = vi-va,
                  vvb = v-vb,
                  vivb = vi-vb;

            double alpha = acos(vva | viva); // Angle CAD
            double beta  = acos(vvb | vivb); // Angle CBD

            coeff = cotan(alpha)+cotan(beta);
        }
        else
            coeff = 1;

        sum += (coeff*(vi-v));
    }

    return poids*sum;
}
void MainWindow::laplaceAll(MyMesh* _mesh)
{
//    if (ui->checkBox_Matrix->isChecked())
//        matriceLB(_mesh);                 // Affichage de la matrice pour le débug

    for (unsigned i = 0 ; i < ui->spinBox_Iterations->value() ; ++i)
    {
        std::map<unsigned, OpenMesh::Vec3f> temp;

        // Pour chaque point, récupère l'application de l'opérateur de Laplace-Beltrami
        for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
            temp[curVert->idx()] = laplaceBeltrami(_mesh,curVert->idx());

        // Applique le résultat de l'opérateur de LB pondéré par des valeurs H et Lambda sur chaque point :
        // vert = vert + LB(vert) * coeff
        for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
            _mesh->set_point(curVert, ui->spinBox_H->value()*ui->spinBox_Lambda->value()*temp[curVert->idx()]+_mesh->point(curVert));

        updateProgressBar((i+1)*100/(ui->spinBox_Iterations->value()+1));
    }
}


/* --------------------------------------------------------------------------------- */
void MainWindow::updateProgressBar(int value)
{
    ui->progressBar->setValue(value);
    if (value == 100)
        ui->progressBar->setVisible(false);
    else
        ui->progressBar->setVisible(true);
    qApp->processEvents();
}

float MainWindow::faceArea(MyMesh* _mesh, int faceID)
{
    FaceHandle fh = _mesh->face_handle ( faceID );

    std::vector<VertexHandle> vertexes;

    MyMesh::FaceVertexIter fh_v = _mesh->fv_iter(fh);
    for(; fh_v.is_valid(); ++fh_v)
        vertexes.push_back ( *fh_v );

    float valuesAB[3];
    valuesAB[0] = _mesh->point(vertexes[1])[0] - _mesh->point(vertexes[0])[0];
    valuesAB[1] = _mesh->point(vertexes[1])[1] - _mesh->point(vertexes[0])[1];
    valuesAB[2] = _mesh->point(vertexes[1])[2] - _mesh->point(vertexes[0])[2];

    float valuesAC[3];
    valuesAC[0] = _mesh->point(vertexes[2])[0] - _mesh->point(vertexes[0])[0];
    valuesAC[1] = _mesh->point(vertexes[2])[1] - _mesh->point(vertexes[0])[1];
    valuesAC[2] = _mesh->point(vertexes[2])[2] - _mesh->point(vertexes[0])[2];

    VectorT<float, 3> vectorAB(valuesAB);
    VectorT<float, 3> vectorAC(valuesAC);

    VectorT<float, 3> product = vectorAB * vectorAC;
    float norm = product.norm();

    return norm / 2.0f;
}

float MainWindow::baryArea(MyMesh* _mesh, int vertID){
    float baryArea = 0;
    // Airebary_vertID = somme(Aire barycentrique des faces adjacentes à vertID)
    VertexHandle vh = _mesh->vertex_handle ( vertID );
    MyMesh::VertexFaceIter vf = _mesh->vf_iter ( vh );
    for ( ; vf.is_valid ( ) ; ++vf )
    {
        FaceHandle current = *vf;
        baryArea += faceArea ( _mesh , current.idx( ) );
    }
    return baryArea / 3.0f;
}

double MainWindow::wij(MyMesh* _mesh, int edgeID)
{
    // Récupère les points et faces liées à l'arète
    MyMesh::EdgeHandle eh = _mesh->edge_handle(edgeID);

    MyMesh::HalfedgeHandle heh0 = _mesh->halfedge_handle(eh, 0);
    MyMesh::HalfedgeHandle heh1 = _mesh->halfedge_handle(eh, 1);

    MyMesh::VertexHandle vh1 = _mesh->to_vertex_handle(heh0);
    MyMesh::VertexHandle vh2 = _mesh->to_vertex_handle(heh1);

    MyMesh::FaceHandle fh1 = _mesh->face_handle(heh0);
    MyMesh::FaceHandle fh2 = _mesh->face_handle(heh1);

    // Si l'arête est en bordure, on ne traite qu'une face
    if ( fh2.idx ( ) > _mesh->n_faces ( ) )
        fh2 = fh1;

    // Récupère les deux autres points des faces fh1 et fh2
    MyMesh::VertexHandle vh3, vh4;

    for (MyMesh::FaceVertexIter curVert = _mesh->fv_iter(fh1) ; curVert.is_valid() ; curVert++)
    {
        MyMesh::VertexHandle vht = *curVert;
        if (vht != vh1 && vht != vh2)
            vh3 = vht;
    }

    for (MyMesh::FaceVertexIter curVert = _mesh->fv_iter(fh2) ; curVert.is_valid() ; curVert++)
    {
        MyMesh::VertexHandle vht = *curVert;
        if (vht != vh1 && vht != vh2)
            vh4 = vht;
    }

    Vec3f v  = _mesh->point(vh1),
          vi = _mesh->point(vh2),
          va = _mesh->point(vh3),
          vb = _mesh->point(vh4);

    Vec3f vva = v-va,
          viva = vi-va,
          vvb = v-vb,
          vivb = vi-vb;

    double alpha = acos(vva | viva);
    double beta  = acos(vvb | vivb);

    return cotan(alpha)+cotan(beta);
}

void MainWindow::matriceLB (MyMesh* _mesh)
{
    double* matrice = new double[_mesh->n_vertices()*_mesh->n_vertices()];

    for (unsigned i = 0 ; i < _mesh->n_vertices()*_mesh->n_vertices() ; ++i)
        matrice[i] = 0;

    for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
    {
        double somme = 0;
        for (MyMesh::VertexEdgeIter curEdge = _mesh->ve_iter(curVert) ; curEdge.is_valid() ; curEdge++)
        {
            MyMesh::EdgeHandle eh = *curEdge;

            MyMesh::HalfedgeHandle heh0 = _mesh->halfedge_handle(eh, 0);
            VertexHandle vert2 = (_mesh->to_vertex_handle(heh0) == curVert?
                                     _mesh->from_vertex_handle(heh0)
                                    :_mesh->to_vertex_handle(heh0));
            matrice[vert2.idx()*_mesh->n_vertices()+curVert->idx()] = wij(_mesh,curEdge->idx());
            somme += matrice[curVert->idx() * _mesh->n_vertices() + vert2.idx()];
        }
        matrice[curVert->idx() * _mesh->n_vertices() + curVert->idx()] = -somme;
    }

    QMessageBox::information(this, tr("Lissage"),
                             tr("Sortie vers build-starterLight/Matrice_Laplacien-Beltrami.txt"));

    std::ofstream os ("Matrice_Laplacien-Beltrami.txt");
    for (unsigned j = 0 ; j < _mesh->n_vertices() ; ++j)
    {
        for (unsigned i = 0 ; i < _mesh->n_vertices() ; ++i)
            os << matrice[j*_mesh->n_vertices()+i] << ' ';
        os << '\n';
    }
    os.close();

    delete [] matrice;
}


/* --------------------------------------------------------------------------------- */
/* **** début de la partie boutons et IHM **** */
void MainWindow::on_pushButton_laplace_clicked()
{
    updateProgressBar(0);

    laplaceAll(&mesh);

    // Mise à jour de l'affichage
    mesh.update_normals();
    resetAllColorsAndThickness(&mesh);
    displayMesh(&mesh);

    updateProgressBar(100);
}

void MainWindow::on_pushButton_chargement_clicked()
{
    // fenêtre de sélection des fichiers
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Mesh"), "", tr("Mesh Files (*.ply)"));

    IO::Options ropt;
    ropt += IO::Options::VertexColor;
    // chargement du fichier .ply dans la variable globale "mesh"
    OpenMesh::IO::read_mesh(mesh, fileName.toUtf8().constData(), ropt);

    mesh.update_normals();

    // initialisation des couleurs et épaisseurs (sommets et arêtes) du mesh
    resetAllColorsAndThickness(&mesh);

    // on affiche le maillage
    displayMesh(&mesh);
}

void MainWindow::on_pushButton_sauvegarde_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Open Mesh"), "", tr("Mesh Files (*.ply)"));

    IO::Options ropt;
    ropt += IO::Options::VertexColor;

    OpenMesh::IO::write_mesh(mesh, fileName.toUtf8().constData(), ropt);
}

/* --------------------------------------------------------------------------------- */

/* **** fonctions supplémentaires **** */
// permet d'initialiser les couleurs et les épaisseurs des élements du maillage
void MainWindow::resetAllColorsAndThickness(MyMesh* _mesh)
{
    for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
    {
        _mesh->data(*curVert).thickness = 0;
        //_mesh->set_color(*curVert, MyMesh::Color(0, 0, 0));
    }

    for (MyMesh::FaceIter curFace = _mesh->faces_begin(); curFace != _mesh->faces_end(); curFace++)
    {
        //_mesh->set_color(*curFace, MyMesh::Color(150, 150, 150));
        QVector<MyMesh::Color> cs;
        for (MyMesh::FaceVertexIter currentVertex = _mesh->fv_iter ( curFace ); currentVertex.is_valid(); currentVertex++)
        {
            cs.append(_mesh->color(*currentVertex));
        }
        MyMesh::Color color = MyMesh::Color(0,0,0);
        for (auto& c : cs)
            color += c/cs.length();
       float n = _mesh->normal(*curFace).normalized() | Vec3f(1.0f,1.0f,0.0f);
       if (n < 0.0f) n = 0.0f;
       if (n > 1.0f) n = 1.0f;
       _mesh->set_color(*curFace, color * n);//_mesh->data(*curFace));
    }

    for (MyMesh::EdgeIter curEdge = _mesh->edges_begin(); curEdge != _mesh->edges_end(); curEdge++)
    {
        _mesh->data(*curEdge).thickness = 0;
        _mesh->set_color(*curEdge, MyMesh::Color(0, 0, 0));
    }
}

// charge un objet MyMesh dans l'environnement OpenGL
void MainWindow::displayMesh(MyMesh* _mesh, bool isTemperatureMap, float mapRange)
{
    GLuint* triIndiceArray = new GLuint[_mesh->n_faces() * 3];
    GLfloat* triCols = new GLfloat[_mesh->n_faces() * 3 * 3];
    GLfloat* triVerts = new GLfloat[_mesh->n_faces() * 3 * 3];

    int i = 0;

    if(isTemperatureMap)
    {
        QVector<float> values;

        if(mapRange == -1)
        {
            for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
                values.append(fabs(_mesh->data(*curVert).value));
            qSort(values);
            mapRange = values.at(values.size()*0.8);
            qDebug() << "mapRange" << mapRange;
        }

        float range = mapRange;
        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;

        for (; fIt!=fEnd; ++fIt)
        {
            fvIt = _mesh->cfv_iter(*fIt);
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++;
        }
    }
    else
    {
        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;
        for (; fIt!=fEnd; ++fIt)
        {
            fvIt = _mesh->cfv_iter(*fIt);
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++;
        }
    }


    ui->displayWidget->loadMesh(triVerts, triCols, _mesh->n_faces() * 3 * 3, triIndiceArray, _mesh->n_faces() * 3);

    delete[] triIndiceArray;
    delete[] triCols;
    delete[] triVerts;

    GLuint* linesIndiceArray = new GLuint[_mesh->n_edges() * 2];
    GLfloat* linesCols = new GLfloat[_mesh->n_edges() * 2 * 3];
    GLfloat* linesVerts = new GLfloat[_mesh->n_edges() * 2 * 3];

    i = 0;
    QHash<float, QList<int> > edgesIDbyThickness;
    for (MyMesh::EdgeIter eit = _mesh->edges_begin(); eit != _mesh->edges_end(); ++eit)
    {
        float t = _mesh->data(*eit).thickness;
        if(t > 0)
        {
            if(!edgesIDbyThickness.contains(t))
                edgesIDbyThickness[t] = QList<int>();
            edgesIDbyThickness[t].append((*eit).idx());
        }
    }
    QHashIterator<float, QList<int> > it(edgesIDbyThickness);
    QList<QPair<float, int> > edgeSizes;
    while (it.hasNext())
    {
        it.next();

        for(int e = 0; e < it.value().size(); e++)
        {
            int eidx = it.value().at(e);

            MyMesh::VertexHandle vh1 = _mesh->to_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
            linesVerts[3*i+0] = _mesh->point(vh1)[0];
            linesVerts[3*i+1] = _mesh->point(vh1)[1];
            linesVerts[3*i+2] = _mesh->point(vh1)[2];
            linesCols[3*i+0] = _mesh->color(_mesh->edge_handle(eidx))[0];
            linesCols[3*i+1] = _mesh->color(_mesh->edge_handle(eidx))[1];
            linesCols[3*i+2] = _mesh->color(_mesh->edge_handle(eidx))[2];
            linesIndiceArray[i] = i;
            i++;

            MyMesh::VertexHandle vh2 = _mesh->from_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
            linesVerts[3*i+0] = _mesh->point(vh2)[0];
            linesVerts[3*i+1] = _mesh->point(vh2)[1];
            linesVerts[3*i+2] = _mesh->point(vh2)[2];
            linesCols[3*i+0] = _mesh->color(_mesh->edge_handle(eidx))[0];
            linesCols[3*i+1] = _mesh->color(_mesh->edge_handle(eidx))[1];
            linesCols[3*i+2] = _mesh->color(_mesh->edge_handle(eidx))[2];
            linesIndiceArray[i] = i;
            i++;
        }
        edgeSizes.append(qMakePair(it.key(), it.value().size()));
    }

    ui->displayWidget->loadLines(linesVerts, linesCols, i * 3, linesIndiceArray, i, edgeSizes);

    delete[] linesIndiceArray;
    delete[] linesCols;
    delete[] linesVerts;

    GLuint* pointsIndiceArray = new GLuint[_mesh->n_vertices()];
    GLfloat* pointsCols = new GLfloat[_mesh->n_vertices() * 3];
    GLfloat* pointsVerts = new GLfloat[_mesh->n_vertices() * 3];

    i = 0;
    QHash<float, QList<int> > vertsIDbyThickness;
    for (MyMesh::VertexIter vit = _mesh->vertices_begin(); vit != _mesh->vertices_end(); ++vit)
    {
        float t = _mesh->data(*vit).thickness;
        if(t > 0)
        {
            if(!vertsIDbyThickness.contains(t))
                vertsIDbyThickness[t] = QList<int>();
            vertsIDbyThickness[t].append((*vit).idx());
        }
    }
    QHashIterator<float, QList<int> > vitt(vertsIDbyThickness);
    QList<QPair<float, int> > vertsSizes;

    while (vitt.hasNext())
    {
        vitt.next();

        for(int v = 0; v < vitt.value().size(); v++)
        {
            int vidx = vitt.value().at(v);

            pointsVerts[3*i+0] = _mesh->point(_mesh->vertex_handle(vidx))[0];
            pointsVerts[3*i+1] = _mesh->point(_mesh->vertex_handle(vidx))[1];
            pointsVerts[3*i+2] = _mesh->point(_mesh->vertex_handle(vidx))[2];
            pointsCols[3*i+0] = _mesh->color(_mesh->vertex_handle(vidx))[0];
            pointsCols[3*i+1] = _mesh->color(_mesh->vertex_handle(vidx))[1];
            pointsCols[3*i+2] = _mesh->color(_mesh->vertex_handle(vidx))[2];
            pointsIndiceArray[i] = i;
            i++;
        }
        vertsSizes.append(qMakePair(vitt.key(), vitt.value().size()));
    }

    ui->displayWidget->loadPoints(pointsVerts, pointsCols, i * 3, pointsIndiceArray, i, vertsSizes);

    delete[] pointsIndiceArray;
    delete[] pointsCols;
    delete[] pointsVerts;
}


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    vertexSelection = -1;
    edgeSelection = -1;
    faceSelection = -1;

    modevoisinage = false;

    ui->setupUi(this);
    ui->progressBar->setVisible(false);
    ui->checkBox_Matrix->setVisible(false);
    ui->comboBox_Laplace->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}
